import { Injectable } from '@angular/core';
import { Vector2 } from '../interface/vector2';
import { Team } from '../marble/team';

@Injectable({
    providedIn: 'root'
})
export class CalculationsService {
    isEven(n:number):boolean {
        return n % 2 == 0;
     }

    isOdd(n:number):boolean {
        return Math.abs(n % 2) == 1;
    }

    findNearestPointOnALine(point:Vector2, lineStart:Vector2, lineEnd:Vector2):Vector2 {
        const atob:Vector2 = new Vector2(lineEnd.x - lineStart.x, lineEnd.y - lineStart.y);
        const atop:Vector2 = new Vector2(point.x - lineStart.x, point.y - lineStart.y);
        const len:number = atob.x * atob.x + atob.y * atob.y;
        const dot:number = atop.x * atob.x + atop.y * atob.y;
        const t:number = Math.min( 1, Math.max( 0, dot / len ) );

        return new Vector2(lineStart.x + atob.x * t, lineStart.y + atob.y * t);
    }

    findPointOnALine(lineStart:Vector2, lineEnd:Vector2, distance:number):Vector2 {
        const xLength:number = lineEnd.x - lineStart.x;
        const yLength:number = lineEnd.y - lineStart.y;
        const hypotenuseLength:number = Math.sqrt(Math.pow(xLength,2) + Math.pow(yLength,2));
        // Determine the ratio between they shortened value and the full hypotenuse.
        const ratio:number = distance / hypotenuseLength;
        const newXLen:number = xLength * ratio;
        const newYLen:number = yLength * ratio;

        // Fix: Never return a NaN
        if(ratio === Infinity) {
            return lineStart;
        }

        return new Vector2(lineStart.x + newXLen, lineStart.y + newYLen);
    }

    numberToHex(value:number):string {
        let result:string;

        value = value < 0 ? 0xFFFFFFFF + value + 1 : value;
        result = value.toString(16).toUpperCase();

        while(result.length < 6) {
            result = `0${result}`;
        }

        return `#${result}`;
    }

    getScore(settingsService:any):number[] {
        const score:number[] = [];
        const teamIndex:any = {};
        const teams:Team[] = settingsService.activeMatch.teams;

        teams.forEach((team, i) => {
            teamIndex[team.id] = i;
            score.push(0);
        });

        settingsService.marbles.forEach(marble => {
            if(!marble.hasLeftTheField) {
                score[teamIndex[marble.team.id]]++;
            }
        });

        return score;
    }

    getHighestOtherValue(values:number[], ignoredValue?:number):number {
        let highestValue:number = -Infinity;
        let foundIgnoreValue:boolean = false;

        values.forEach(value => {
            if(!foundIgnoreValue && ignoredValue && ignoredValue === value) {
                foundIgnoreValue = true;
            }
            else {
                highestValue = value > highestValue ? value : highestValue;
            }
        });

        return highestValue;
    }
}
