import { Injectable, NgZone, OnDestroy } from '@angular/core';
import * as PIXI from 'pixi.js';
import { SettingsService } from '../settings/settings.service';
import { Vector2 } from '../interface/vector2';
import { Marble } from '../marble/marble';
import { Square } from '../interface/square';

@Injectable({
    providedIn: 'root'
})
export class RendererService implements OnDestroy {
    app:PIXI.Application;
    pixiElements:any[] = [];
    marbles:PIXI.Graphics[] = [];

    constructor(private ngZone:NgZone, private settings:SettingsService) {
        this.createCanvas(ngZone);
    }

    createCanvas(ngZone:NgZone):void {
        /**
         * Make Angular and Pixi get along nicely. See also:
         * https://studiolacosanostra.github.io/2019/02/11/How-to-run-PIXI-in-angular-app/
         */
        ngZone.runOutsideAngular(() => {
            this.app = new PIXI.Application({
                width: this.settings.stage.width,
                height: this.settings.stage.height,
                antialias: true,
                transparent: true,
                view: document.querySelector('[data-canvas]')
            });
        });
    }

    addElement(element:any):void {
        this.app.stage.addChild(element);
        this.pixiElements.push(element);
    }

    createTrack():void {
        this.createSquare(this.settings.fieldBorders);
        this.createSquare(this.settings.field);
        this.createSquare(this.settings.slope1);
        this.createSquare(this.settings.slope2);

        this.settings.startAreas.forEach(startArea => {
            this.createSquare(startArea);
        });

        this.showStartArea();
    }

    showStartArea():void {
        this.setStartAreaVisibility(1);
    }

    hideStartArea():void {
        this.setStartAreaVisibility(0);
    }

    setStartAreaVisibility(amount:number):void {
        this.settings.startAreas.forEach(startArea => {
            startArea.options.graphics.alpha = amount;
        });
    }

    createSquare(objectSettings:Square):void {
        const graphics:PIXI.Graphics = new PIXI.Graphics();

        graphics.lineStyle(0);
        graphics.beginFill(objectSettings.options.color);
        graphics.drawRect(objectSettings.x, objectSettings.y, objectSettings.width, objectSettings.height);

        this.addElement(graphics);
        objectSettings.setOptions('graphics', graphics);
    }

    addMarble(marble:Marble):void {
        const graphics = new PIXI.Graphics();
        graphics.x = marble.position.x;
        graphics.y = marble.position.y;
        marble.graphics = graphics;

        graphics.lineStyle(0);
        graphics.beginFill(marble.color, 1);
        graphics.drawCircle(0, 0, marble.radius);
        graphics.endFill();

        this.addElement(graphics);
        this.marbles.push(graphics);
    }

    updateMarble(marble:Marble):void {
        marble.graphics.x = marble.position.x;
        marble.graphics.y = marble.position.y;
    }

    clearMarbles():void {
        this.marbles.forEach(element => {
            element.destroy({texture: true, baseTexture: true});
        });

        this.marbles = [];
    }

    clearStage():void {
      this.pixiElements.forEach(element => {
          element.destroy({texture: true, baseTexture: true});
      });

      this.pixiElements = [];
    }
     
    ngOnDestroy(): void {
        this.clearStage();
        this.app.destroy();
    }
}
