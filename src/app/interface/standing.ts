export class Standing {
    name:string;
    matches:number = 0;
    victoryPoints:number = 0;
    pointDifference:number = 0;
    pointsScored:number = 0;
    pointsLost:number = 0;

    constructor(name:string) {
        this.name = name;
    }

    addMatch() {
        this.matches++;
        return this;
    }

    addVictoryPoints(points:number) {
        this.victoryPoints += points;
        return this;
    }

    addPointDifference(points:number) {
        this.pointDifference += points;
        return this;
    }

    addPointsScored(points:number) {
        this.pointsScored += points;
        return this;
    }

    addPointsLost(points:number) {
        this.pointsLost += points;
        return this;
    }
}