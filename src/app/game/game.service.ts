import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { RendererService } from '../renderer/renderer.service';
import { CollisionService } from '../collision/collision.service';
import { CalculationsService } from '../helpers/calculations.service';
import { CompetitionService } from '../helpers/competition.service';
import { Match } from '../interface/match';
import { Square } from '../interface/square';

@Injectable({
    providedIn: 'root'
})
export class GameService {
    startTime:number;
    currentTime:number;
    lastTime:number = 0;
    interval:number = 1000 / 60;
    isActive:boolean = false;

    constructor(private settings:SettingsService, private renderS:RendererService, private collisions:CollisionService, private calculations:CalculationsService, private competitionService:CompetitionService) {
        renderS.createTrack();
        this.startNewCompetition();

        // Fix bug where browser tab is not active.
        window.addEventListener('blur', () => {
            if(!this.settings.isPaused) {
                this.togglePaused();
            }
        });
    }

    startNewCompetition():void {
        this.settings.matchSchedule = this.competitionService.generate(this.settings.teams);
        this.settings.activeMatchIndex = 0;

        this.startNewGame();
    }

    startNewGame():void {
        this.settings.activeMatch = this.settings.matchSchedule[this.settings.activeMatchIndex];

        this.addMarbles();
        this.renderS.showStartArea();

        setTimeout(() => {
            this.startGame();
        }, 1000);
    }

    addMarbles():void {
        this.settings.marbles = [];

        this.settings.activeMatch.teams.forEach((team, index) => {
            team.addMarbles(this.settings.startAreas[index]);
        });

        this.renderS.clearMarbles();

        this.settings.marbles.forEach(marble => {
            this.renderS.addMarble(marble);
        });
    }

    togglePaused():void {
        this.settings.isPaused = !this.settings.isPaused;

        // Restart game
        if (!this.settings.isPaused) {
            this.startGame();
        }
    };

    startGame():void {
        this.isActive = true;
        this.startTime = Date.now();
        this.lastTime = this.startTime;
        this.settings.countDown = this.settings.countDownSeconds;

        this.renderS.hideStartArea();
        this.gameLoop();
    };

    gameLoop():void {
        if(!this.isActive || this.settings.isPaused) {
            return;
        }

        window.requestAnimationFrame(() => this.gameLoop());

        // Time
        this.currentTime = Date.now();
        this.settings.deltaTime = this.currentTime - this.lastTime;
        this.settings.countDown = this.settings.countDownSeconds - Number(String(this.currentTime - this.startTime).slice(0, -3));

        if (this.settings.deltaTime > this.interval) {
            this.settings.marbles.forEach(marble => {
                // Run marble for one tick
                marble.update();
                this.renderS.updateMarble(marble);
            });

            // Search collisions
            this.collisions.searchCollisions(this.settings.marbles);

            // Time
            this.lastTime = Date.now();
        }

        if(this.settings.countDown <= 0) {
            this.endGame();
        }
    };

    endGame():void {
        const match:Match = this.settings.activeMatch;

        match.result = this.calculations.getScore(this.settings);

        this.isActive = false;

        // Save latest scores
        this.settings.teams.forEach(team => {
            const ownScore:number = match.result[team.id];
            const highestOtherValue:number = this.calculations.getHighestOtherValue(match.result, ownScore);

            team.score = ownScore - highestOtherValue;
        });

        setTimeout(() => {
            this.settings.activeMatchIndex++;

            if(this.settings.activeMatchIndex < this.settings.matchSchedule.length) {
                this.startNewGame();
            }
            else {
                this.endCompetition();
            }

        }, 2000);
    }

    endCompetition():void {
        // TODO
    }
}
