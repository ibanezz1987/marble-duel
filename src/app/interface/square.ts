export class Square {
    width?:number = 0;
    height?:number = 0;
    x?:number = 0;
    y?:number = 0;
    options?:any = {};

    constructor(width?:number, height?:number, x?:number, y?:number, options?:any) {
        this.width = width !== undefined ? width : this.width;
        this.height = height !== undefined ? height : this.height;
        this.x = x !== undefined ? x : this.x;
        this.y = y !== undefined ? y : this.y;
        this.options = options !== undefined ? options : this.options;
    }

    alignCenterTo(x:number, y:number):Square {
        this.x = x - this.width / 2;
        this.y = y - this.height / 2;

        return this;
    }

    alignCenterOf(square:Square):Square {
        this.x = (square.x + square.width / 2) - this.width / 2;
        this.y = (square.y + square.height / 2 ) - this.height / 2;

        return this;
    }

    copy(square:Square):Square {
        return new Square(square.width, square.height, square.x, square.y, JSON.parse(JSON.stringify(square.options)));
    }

    setX(value:any):Square {
        this.x = value;

        return this;
    }

    setOptions(key:string, value:any):Square {
        this.options[key] = value;

        return this;
    }
}