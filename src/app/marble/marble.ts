import { SettingsService } from '../settings/settings.service';
import { Vector2 } from '../interface/vector2';
import { Team } from './team';
import { Square } from '../interface/square';

export class Marble {
    team:Team;
    position:Vector2;
    velocity:Vector2 = new Vector2();
    radius:number;
    color:number = 0xeeeeee;
    graphics:PIXI.Graphics;
    hasLeftTheField:boolean = false;    // Lost its point
    hasLeftSlope:boolean = false;       // Has entered the field for the first time
    hasLeftTheStage:boolean = false;     // Is out of view

    constructor(private settings:SettingsService, team:Team, relativePosition:Vector2, startArea:Square) {
        this.radius = settings.marbleSettings.radius;
        this.team = team;
        this.color = team.color ? team.color : this.color;

        this.position = this.getAbsolutePosition(relativePosition, startArea);
    }

    getAbsolutePosition(relativePosition:Vector2, startArea:Square):Vector2 {
        const isReversed:boolean = startArea.options.isReversed;
        const x:number = startArea.x + (isReversed ? startArea.width - relativePosition.x : relativePosition.x)
        const y:number = startArea.y + (isReversed ? startArea.height - relativePosition.y : relativePosition.y)
        
        return new Vector2(x, y);
    }

    update():void {
        if(!this.hasLeftTheStage) {
            this.position = this.position.add(this.velocity.multiply(this.settings.deltaTime / 1000));
            this.velocity.multiplyWith(1 - this.settings.friction);
        }
    }
}
