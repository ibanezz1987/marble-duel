import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { CalculationsService } from './calculations.service';
import { Match } from '../interface/match';
import { Team } from '../marble/team';
import { Standing } from '../interface/standing';

@Injectable({
    providedIn: 'root'
})
export class CompetitionService {
    cachedStandings:Standing[] = [];
    latestMatch:number = -1;

    constructor(private settings:SettingsService, private calculations:CalculationsService) {}

    generate(teams:Team[]):Match[] {
        let schedule:Match[] = [];
        const isOdd:boolean = this.calculations.isOdd(teams.length);
        const totalRounds:number = isOdd ? teams.length : teams.length - 1;
        const totalMatchesPerRound:number = Math.ceil(teams.length / 2);
        let homeGroup:any[] = teams.slice(0, Math.ceil(teams.length / 2));
        let awayGroup:any[] = teams.slice(Math.ceil(teams.length / 2)).reverse();

        if(isOdd) {
            awayGroup.push(false);
        }

        // Create rounds
        for (let i = 0; i < totalRounds; i++) {
            // Create matches
            for (let j = 0; j < totalMatchesPerRound; j++) {
                const homeTeam:Team = homeGroup[j];
                const awayTeam:Team = awayGroup[j];

                if(homeTeam && awayTeam) {
                    schedule.push(new Match([homeTeam, awayTeam]));
                }
            }

            // Move last team from homeGroup to end of awayGroup
            awayGroup.push(homeGroup.pop());

            // Move first team from awayGroup to 2nd position of homeGroup
            homeGroup.splice(1, 0, awayGroup.shift());
        }

        return schedule;
    }

    getStandings(schedule:Match[]):any {
        let standings:Standing[] = [];

        // Use cached standings whenever possible
        if(this.settings.activeMatchIndex - 1 === this.latestMatch) {
            return this.cachedStandings;
        }

        // Apply scores
        schedule.forEach((match:Match, matchIndex:number) => {
            if(match.result.length) {
                this.latestMatch = matchIndex;

                match.teams.forEach((team:Team, i:number) => {
                    const standing:Standing = standings[team.id] ? standings[team.id] : new Standing(team.name);
                    const pointsScored:number = match.result[i];
                    const pointsLost:number = this.calculations.getHighestOtherValue(match.result, pointsScored);
                    const pointDifference:number = pointsScored - pointsLost;
                    const victoryPoints:number = pointDifference > 0 ? 3 : pointDifference === 0 ? 1 : 0;

                    standings[team.id] = standing.addMatch().addVictoryPoints(victoryPoints).addPointDifference(pointDifference).addPointsScored(pointsScored).addPointsLost(pointsLost);
                });
            }
        });

        // Remove empty
        standings = standings.filter(el => { return el != null });

        // Sorting
        standings = this.sortStandings(standings);

        // Save a cached version
        this.cachedStandings = standings;

        return standings;
    }

    sortStandings(standings:Standing[]):Standing[] {
        return standings.sort((a, b) => {
            if(b.victoryPoints === a.victoryPoints && b.pointDifference === a.pointDifference) {
                return b.pointsScored - a.pointsScored;
            }
            
            if(b.victoryPoints === a.victoryPoints) {
                return b.pointDifference - a.pointDifference;
            }

            return b.victoryPoints - a.victoryPoints;
        });
    }
}
