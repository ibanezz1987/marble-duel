import { Injectable } from '@angular/core';
import { SettingsService } from '../settings/settings.service';
import { CalculationsService } from '../helpers/calculations.service';
import { Marble } from '../marble/marble';
import { Vector2 } from '../interface/vector2';
import { Square } from '../interface/square';
import { Line } from '../interface/line';

// Intersection class
// https://github.com/davidfig/intersects
declare const Intersects:any;

@Injectable({
    providedIn: 'root'
})
export class CollisionService {
    constructor(private settings:SettingsService, private calculations:CalculationsService) { }

    searchCollisions(marbles:Marble[]):void {
        marbles.forEach((marble, i) => {
            this.searchSlopeCollisions(marble);
            this.searchSlopeEdgeCollisions(marble);
            this.searchFieldEdgeCollisions(marble);
            this.searchStageEdgeCollisions(marble);

            marbles.forEach((otherMarble, j) => {
                // Search kart collisions
                if(i > j) {
                    this.searchMarbleCollisions(marble, otherMarble);
                }
            });
        });
    }

    searchSlopeCollisions(marble:Marble):void {
        let hasCollisionOnASlope:boolean = false;

        this.settings.slopes.forEach(slope => {
            const isColliding:boolean = Intersects.boxCircle(slope.x, slope.y, slope.width, slope.height, marble.position.x, marble.position.y, marble.radius);
            hasCollisionOnASlope = isColliding ? true : hasCollisionOnASlope;

            if(isColliding && !marble.hasLeftSlope) {
                this.handleSlopeCollisions(marble, slope);
            }
        });

        if(!hasCollisionOnASlope) {
            marble.hasLeftSlope = true;
        }
    }

    handleSlopeCollisions(marble:Marble, slope:Square):void {
        const slopeDirection:string = slope.options.direction;
        const slopeVelocity:Vector2 = new Vector2(slopeDirection === 'left' ? -2 : 2, 0);

        marble.velocity = slopeVelocity.addTo(marble.velocity);
    }

    searchSlopeEdgeCollisions(marble:Marble):void {
        if(marble.hasLeftSlope) {
            this.settings.slopes.forEach(slope => {
                const isColliding:boolean = Intersects.boxCircle(slope.x, slope.y, slope.width, slope.height, marble.position.x, marble.position.y, marble.radius);

                if(isColliding) {
                    const slopeDirection:string = slope.options.direction;
                    const edge:Line = slopeDirection === 'left' ? new Line(slope.x, slope.y, slope.x, slope.y + slope.height) : new Line(slope.x + slope.width, slope.y, slope.x + slope.width, slope.y + slope.height);

                    this.handleStageCollisions(marble, edge);
                }
            });
        }
    }

    searchFieldEdgeCollisions(marble:Marble):void {
        if(marble.hasLeftSlope) {
            const field:Square = this.settings.field;
            const slope:Square = this.settings.slope;
            const slopeStartY:number = field.y + field.height / 2 - slope.height / 2;
            const slopeEndY:number = field.y + field.height / 2 + slope.height / 2;
            const fieldEdges:Line[] = [
                new Line(field.x, field.y, field.x + field.width, field.y),
                new Line(field.x, field.y + field.height, field.x + field.width, field.y + field.height),
                new Line(field.x, field.y, field.x, slopeStartY),
                new Line(field.x, slopeEndY, field.x, field.y + field.height),
                new Line(field.x + field.width, field.y, field.x + field.width, slopeStartY),
                new Line(field.x + field.width, slopeEndY, field.x + field.width, field.y + field.height),
            ];

            fieldEdges.forEach(edge => {
                const isColliding:boolean = Intersects.circleLine(marble.position.x, marble.position.y, this.settings.marbleSettings.radius, edge.start.x, edge.start.y, edge.end.x, edge.end.y);

                if(isColliding) {
                    // TODO: Don't use getTotal() but use the velocity in the edge's direction
                    if(!marble.hasLeftTheField && marble.velocity.getTotal() > this.settings.exitFieldVelocity) {
                        marble.hasLeftTheField = true;
                    }
                    else if(!marble.hasLeftTheField) {
                        marble.velocity = marble.velocity.multiply(1 - this.settings.fieldEdgeFriction);
                        this.handleStageCollisions(marble, edge);
                    }
                }
            });
        }
    }

    searchStageEdgeCollisions(marble:Marble):void {
        const radius:number = this.settings.marbleSettings.radius;
        const stage:Square = this.settings.stage;

        if(marble.position.x < 0 - radius || marble.position.x > stage.width + radius || marble.position.y < 0 - radius || marble.position.y > stage.height + radius) {
            marble.hasLeftTheStage = true;
            marble.hasLeftTheField = true;
        }
    }

    handleStageCollisions(marble:Marble, edge:Line):void {
        if(marble.hasLeftTheField) {
            return;
        }

        let power:number = (Math.abs(marble.velocity.x) + Math.abs(marble.velocity.y)) * 2;
        power = power * 0.00482;

        const nearestPoint:Vector2 = this.calculations.findNearestPointOnALine(marble.position, edge.start, edge.end);
        const opposite:number = marble.position.y - nearestPoint.y;
        const adjacent:number = marble.position.x - nearestPoint.x;
        const rotation:number = Math.atan2(opposite, adjacent);

        const VelocityChange:Vector2 = new Vector2(90 * Math.cos(rotation) * power, 90 * Math.sin(rotation) * power);
        marble.velocity = marble.velocity.addTo(VelocityChange);
        marble.velocity.multiplyWith(1 - this.settings.collisionFriction);
    }

    searchMarbleCollisions(marble:Marble, otherMarble:Marble):void {
        if(marble.hasLeftTheStage || otherMarble.hasLeftTheStage) {
            return;
        }

        const distance:number = marble.position.distanceFrom(otherMarble.position);
        const isColliding:boolean = distance < marble.radius * 2;

        if(isColliding){
            this.handleMarbleCollision(marble, otherMarble);
        }
    }

    handleMarbleCollision(marble:Marble, otherMarble:Marble):void {
        let power:number = (Math.abs(marble.velocity.x) + Math.abs(marble.velocity.y)) + 
                           (Math.abs(otherMarble.velocity.x) + Math.abs(otherMarble.velocity.y));
        power = power * 0.00482;

        const opposite:number = marble.position.y - otherMarble.position.y;
        const adjacent:number = marble.position.x - otherMarble.position.x;
        const rotation:number = Math.atan2(opposite, adjacent);

        const velocity1:Vector2 = new Vector2(90 * Math.cos(rotation) * power, 90 * Math.sin(rotation) * power);
        marble.velocity = marble.velocity.addTo(velocity1);
        marble.velocity.multiplyWith(1 - this.settings.collisionFriction);

        const velocity2:Vector2 = new Vector2(90 * Math.cos(rotation + Math.PI) * power, 90 * Math.sin(rotation + Math.PI) * power);
        otherMarble.velocity = otherMarble.velocity.addTo(velocity2);
        otherMarble.velocity.multiplyWith(1 - this.settings.collisionFriction);
    }
}
