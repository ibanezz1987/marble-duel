import { SettingsService } from '../settings/settings.service';
import { Vector2 } from '../interface/vector2';
import { Marble } from './marble';
import { Square } from '../interface/square';

export class Team {
    id:number;
    name:string;
    color:number;
    score:number;
    marblePositions:Vector2[] = [];

    constructor(id:number, name:string, color:number = 0xeeeeee, private settings:SettingsService) {
        this.id = id;
        this.name = name;
        this.color = color;
    }

    addMarbles(startArea:Square):void {
        if(this.marblePositions.length === 0 || this.score < 1) {
            this.marblePositions = this.getRandomPositions(startArea);

            console.log(this.name, this.score, 'get new positions');
        }

        for (let i = 0; i < this.settings.teamSettings.totalMarbles; i++) {
            const marble:Marble = new Marble(this.settings, this, this.marblePositions[i], startArea);

            this.settings.marbles.push(marble);
        }
    }

    getRandomPositions(startArea:Square):Vector2[] {
        const randomPositions:Vector2[] = [];

        for (let i = 0; i < this.settings.teamSettings.totalMarbles; i++) {
            let randomPosition:Vector2;

            while(!randomPosition || !this.isUniquePostion(randomPosition, randomPositions)) {
                randomPosition = this.getRandomPosition(startArea);
            }

            randomPositions.push(randomPosition);
        }

        return randomPositions;
    }

    getRandomPosition(startArea:Square):Vector2 {
        const randomX:number = Math.random() * (startArea.width - this.settings.marbleSettings.radius * 2) + this.settings.marbleSettings.radius;
        const randomY:number = Math.random() * (startArea.height - this.settings.marbleSettings.radius * 2) + this.settings.marbleSettings.radius;

        return new Vector2(randomX, randomY);
    }

    isUniquePostion(position:Vector2, occupiedPositions:Vector2[]) {
        let isUniquePostion:boolean = true;
        const diameter:number = this.settings.marbleSettings.radius * 2;

        occupiedPositions.forEach((occupiedPosition:Vector2) => {
            if(Math.abs(occupiedPosition.x - position.x) < diameter && Math.abs(occupiedPosition.y - position.y) < diameter) {
                isUniquePostion = false;
            }
        });

        return isUniquePostion;
    }
}
