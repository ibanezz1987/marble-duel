import { Vector2 } from './vector2';

export class Line {
    start:Vector2 = new Vector2();
    end:Vector2 = new Vector2();

    constructor(x1:number, y1:number, x2:number, y2:number) {
        this.start.x = x1;
        this.start.y = y1;
        this.end.x = x2;
        this.end.y = y2;
    }
}