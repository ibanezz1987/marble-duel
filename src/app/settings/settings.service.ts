import { Injectable } from '@angular/core';
import { Square } from '../interface/square';
import { Circle } from '../interface/circle';
import { Team } from '../marble/team';
import { Marble } from '../marble/marble';
import { Match } from '../interface/match';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {
    // Physics
    deltaTime:number;
    friction:number = 0.001;
    collisionFriction:number = 0.03;
    fieldEdgeFriction:number = 0.5;
    exitFieldVelocity:number = 100;

    // Game
    countDownSeconds:number = 10;
    countDown:number = this.countDownSeconds;
    isPaused:boolean = false;
    matchSchedule:Match[] = [];
    activeMatchIndex:number;
    activeMatch:Match;

    // Team & marble settings
    teamSettings:any = { totalMarbles: 5 };
    marbleSettings:Circle = { radius: 9 };

    // Stage elements
    stage:Square = new Square(1100, 600);
    field:Square = new Square(600, 400, 0, 0, {color: 0xffffff}).alignCenterOf(this.stage);
    fieldBorders:Square = new Square(620, 420, 0, 0, {color: 0x000000}).alignCenterOf(this.stage);
    slope:Square = new Square(250, 150, 0, 0).alignCenterOf(this.stage);
    slope1:Square = new Square().copy(this.slope).setX(0).setOptions('color', 0xeeeeee).setOptions('direction', 'right');
    slope2:Square = new Square().copy(this.slope).setX(this.stage.width - this.slope.width).setOptions('color', 0xeeeeee).setOptions('direction', 'left');
    startArea:Square = new Square(100, 100, 0, 0, {color: 0xdddddd}).alignCenterOf(this.stage);
    startAreas:Square[] = [
        new Square().copy(this.startArea).setX(0).setOptions('isReversed', false),
        new Square().copy(this.startArea).setX(this.stage.width - this.startArea.width).setOptions('isReversed', true),
    ];

    // Instances
    slopes:Square[] = [this.slope1, this.slope2];
    teams:Team[] = [
        new Team(0, 'Apple', 0xff4444, this),
        new Team(1, 'Orange', 0xffa500, this),
        new Team(2, 'Lime', 0xcddc39, this),
        new Team(3, 'Kiwi', 0x4caf50, this),
        new Team(4, 'Blueberry', 0x2196f3, this),
        new Team(5, 'Blackberry', 0x222222, this),
    ];
    marbles:Marble[] = [];
}
