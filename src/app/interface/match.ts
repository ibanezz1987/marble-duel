import { Team } from '../marble/team';

export class Match {
    teams:Team[];
    result:number[] = [];

    constructor(teams:Team[]) {
        this.teams = teams;
    }
}