import { Component } from '@angular/core';
import { GameService } from '../game/game.service';
import { SettingsService } from '../settings/settings.service';
import { CalculationsService } from '../helpers/calculations.service';
import { Team } from '../marble/team';
import { Standing } from '../interface/standing';
import { CompetitionService } from '../helpers/competition.service';

@Component({
    selector: 'app-ui',
    templateUrl: './ui.component.html',
    styleUrls: ['./ui.component.scss']
})
export class UiComponent {
    constructor(private game:GameService, private settings:SettingsService, private calculations:CalculationsService, private competitionService:CompetitionService) { }

    getCountdown():number {
        return this.settings.countDown;
    }

    getTeams():Team[] {
        const teams:any[] = [];

        this.settings.activeMatch.teams.forEach(team => {
            const object:any = {
                name: team.name,
                color: this.calculations.numberToHex(team.color)
            };
            
            teams.push(object);
        });

        return teams;
    }

    getScore():number[] {
        return this.calculations.getScore(this.settings);
    }

    isPaused():boolean {
        return this.settings.isPaused;
    }

    togglePaused():void {
        this.game.togglePaused();
    }

    getStandings():Standing[] {
        return this.competitionService.getStandings(this.settings.matchSchedule);
    }
}
